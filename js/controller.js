function renderDSSV(dssv) {
    var contenHTML = "";
    for (let i = 0; i < dssv.length; i++) {
        var sv = dssv[i];
        var conten = `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${sv.ngaySinh}</td>
        <td>${sv.khoaHoc}</td>
        <td>${sv.tinhDTB()}</td>
        <td>
        <button onclick ="xoaSinhVien('${sv.ma}')" type="button" class="btn btn-outline-primary">Xoá</button>
        <button onclick ="suaSinhVien('${sv.ma}')" type="button" class="btn btn-outline-primary">Sửa</button>
        </td>
        </tr>`
        contenHTML = contenHTML + conten;
    }
    document.getElementById("tbodySinhVien").innerHTML = contenHTML;
}
function showThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtNgaySinh").value = sv.ngaySinh;
    document.getElementById("khSV").value = sv.khoaHoc;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;

}
function layThongTinTuForm() {
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var Pass = document.getElementById("txtPass").value;
    var ngaySinh = document.getElementById("txtNgaySinh").value;
    var e = document.getElementById("khSV");
    var khoaHoc = e.options[e.selectedIndex].text;
    var toan = document.getElementById("txtDiemToan").value * 1;
    var ly = document.getElementById("txtDiemLy").value * 1;
    var hoa = document.getElementById("txtDiemHoa").value * 1;
    return new SinhVien(ma, ten, email, Pass, ngaySinh, khoaHoc, toan, ly, hoa)

}