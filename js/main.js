var dssv = [];
var datajson = localStorage.getItem("ds");
if (datajson != null) {
    // map
    dssv = JSON.parse(datajson).map(function (item) {
        return new SinhVien(item.ma, item.ten, item.email, item.matKhau, item.ngaySinh, item.khoaHoc, item.toan, item.ly, item.hoa);
    });
    renderDSSV(dssv);
}


function themSinhVien() {
    var sv = layThongTinTuForm();
    dssv.push(sv);
    renderDSSV(dssv);
    var datajson = JSON.stringify(dssv);
    localStorage.setItem("ds", datajson);
    // reset
    document.getElementById("formQLSV").reset();


}

function xoaSinhVien(id) {
    var index = dssv.findIndex(function (item) {
        return item.ma == id;
    })
    dssv.splice(index, 1);
    renderDSSV(dssv);
    var datajson = JSON.stringify(dssv);
    localStorage.setItem("ds", datajson);

}
function suaSinhVien(id) {
    var index = dssv.findIndex(function (item) {
        return item.ma == id;
    })
    showThongTinLenForm(dssv[index]);
    // renderDSSV(dssv);

}

function reset() {
    document.getElementById("formQLSV").reset();
}
function capNhatSinhVien() {
    var sv = layThongTinTuForm();
    xoaSinhVien(sv.ma);
    dssv.push(sv);
    var datajson = JSON.stringify(dssv);
    localStorage.setItem("ds", datajson);
    renderDSSV(dssv);
    // reset
    document.getElementById("formQLSV").reset();
}

// regex;