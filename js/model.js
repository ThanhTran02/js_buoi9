function SinhVien(ma, ten, email, matKhau, ngaySinh, khoaHoc, toan, ly, hoa) {
    this.ma = ma;
    this.ten = ten;
    this.email = email;
    this.matKhau = matKhau;
    this.ngaySinh = ngaySinh;
    this.khoaHoc = khoaHoc;
    this.toan = toan;
    this.ly = ly;
    this.hoa = hoa;
    this.tinhDTB = function () {
        return (toan + ly + hoa) / 3;
    };

}   